/*
Copyright 2019 @foostan
Copyright 2020 Drashna Jaelre <@drashna>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
qmk compile -kb crkbd -km davidroberson3
*/

#include QMK_KEYBOARD_H

// Backspace delete shift
enum custom_keycodes
{
    M_BSDEL = SAFE_RANGE
};

uint8_t del_bspc_lastcode = KC_NO;

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] =
{
  [0] = LAYOUT_split_3x6_3( // Base
  KC_TAB,  KC_Q,  KC_W,  KC_E,  KC_R,  KC_T,                         KC_Y,  KC_U,  KC_I,  KC_O,  KC_P, M_BSDEL,
  KC_ESC,  KC_A,  KC_S,  KC_D,  KC_F,  KC_G,                         KC_H,  KC_J,  KC_K,  KC_L, KC_SCLN, KC_ENT,
  KC_LGUI, KC_Z,  KC_X,  KC_C,  KC_V,  KC_B,                         KC_N,  KC_M, KC_COMM, KC_DOT, KC_SLASH, KC_QUOT,
                              MO(2), KC_LSFT, MO(1),          KC_RALT, KC_SPC, KC_RCTL),
  [1] = LAYOUT_split_3x6_3( // Number
  _______, KC_F9, KC_F10, KC_F11, KC_F12, KC_BSPC,                   KC_KP_ASTERISK, KC_7, KC_8, KC_9, KC_SLASH, _______,
  _______, KC_F5, KC_F6,  KC_F7,  KC_F8,  KC_LALT,                   KC_KP_PLUS,     KC_4, KC_5, KC_6, KC_0, _______,
  _______, KC_F1, KC_F2,  KC_F3,  KC_F4,  KC_LCTL,                   KC_MINUS,       KC_1, KC_2, KC_3, KC_DOT, KC_EQUAL,
                              TG(4), _______, _______,        _______, _______, _______),
  [2] = LAYOUT_split_3x6_3( // Symbol
  _______, KC_EXLM, KC_AT, KC_HASH,  KC_DLR, KC_PERC,                KC_CIRC, KC_AMPR, KC_UP, KC_LPRN, KC_RPRN, _______,
  _______, KC_TILD, MO(3), KC_PIPE, KC_UNDS, KC_PPLS,                KC_PGUP, KC_LEFT, KC_DOWN, KC_RGHT, KC_HOME, _______,
  _______, KC_GRV, KC_ASTR, KC_BSLS, KC_PMNS, KC_EQL,                KC_PGDN, KC_LCBR, KC_RCBR, KC_LBRC, KC_RBRC, KC_END,
                              _______, _______, XXXXXXX,        _______, _______, _______),
  [3] = LAYOUT_split_3x6_3( // Mouse
  _______, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,              XXXXXXX, XXXXXXX, KC_MS_U, XXXXXXX, XXXXXXX, KC_DELETE,
  _______, XXXXXXX, _______, KC_ACL2, KC_BTN1, KC_BTN2,              KC_AUDIO_VOL_UP, KC_MS_L, KC_MS_D, KC_MS_R, XXXXXXX, _______,
  _______, XXXXXXX, XXXXXXX, KC_ACL0, XXXXXXX, XXXXXXX,              KC_AUDIO_VOL_DOWN, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
                              _______, _______, XXXXXXX,        _______, _______, _______),
  [4] = LAYOUT_split_3x6_3( // Starcraft
  _______, KC_Q,  KC_6,  KC_5,  KC_4,  KC_1,             _______, _______, _______, _______, _______, _______,
  _______, KC_A,  KC_S,  KC_D,  KC_F,  KC_2,             _______, _______, _______, _______, _______, _______,
  _______, KC_Z,  KC_X,  KC_C,  KC_V,  KC_3,             _______, _______, _______, _______, _______, _______,
                              KC_LCTL, KC_LSFT, TG(4),        _______, _______, _______)
};

// used as filler for mod keys, to make keycode display convenient 
// KC_SYSTEM_POWER = 0xA5,
#define KC_MOD1 KC_SYSTEM_POWER
// KC_SYSTEM_SLEEP
#define KC_MOD2 KC_SYSTEM_SLEEP
// KC_SYSTEM_WAKE,
#define KC_MOD3 KC_SYSTEM_WAKE

/*
https://text-symbols.com/ascii/
*/

#define _NO 0//249

#define _ENT 20
#define _ESC 19
#define _BCK 17
#define _DEL 16
#define _TAB 9//175
#define _SPC 22

#define _CTR 127//94
#define _SHF 30//24
#define _ALT 4//170
#define _GUI 3//14

#define _UP 24
#define _DWN 25
#define _LFT 27
#define _RGT 26

#define _F1 128
#define _F2 129
#define _F3 130
#define _F4 131
#define _F5 132
#define _F6 133
#define _F7 134
#define _F8 135
#define _F9 136
#define _F10 137
#define _F11 138
#define _F12 139

#define _HOME 11
#define _END 12
#define _PGUP 5
#define _PGDN 6

#define _MO1 192
#define _MO2 193

// MOUSE
#define _MO3 160
#define _LCLK 161
#define _RCLK 162
#define _MAC1 163
#define _MAC2 164

#ifdef OLED_ENABLE

/*
https://github.com/qmk/qmk_firmware/blob/master/docs/feature_oled_driver.md
*/

// BASE
static char keymapCharBASE[8][6] = {
    // Main
    {_TAB, 'Q', 'W', 'E', 'R', 'T'},
    {_ESC, 'A', 'S', 'D', 'F', 'G'},
    {_GUI, 'Z', 'X', 'C', 'V', 'B'},
    {0, 0, 0, _MO2, _SHF, _MO1},
    // Secondary (reversed)
    {_BCK, 'P', 'O', 'I', 'U', 'Y'},
    {_ENT, ';', 'L', 'K', 'J', 'H'},
    {'\'', '/', '.', ',', 'M', 'N'},
    {0, 0, 0, _CTR, _SPC, _ALT}};

// NUM
static char keymapCharNUM[8][6] = {
    // Main
    {_TAB, _F9, _F10, _F11, _F12, _BCK},
    {_ESC, _F5, _F6, _F7, _F8, _ALT},
    {_GUI, _F1, _F2, _F3, _F4, _CTR},
    {0, 0, 0, _MO1, _SHF, _MO1},
    // Secondary (reversed)
    {_BCK, '/', '9', '8', '7', '*'},
    {_ENT, '0', '6', '5', '4', '+'},
    {'=', '.', '3', '2', '1', '-'},
    {0, 0, 0, _CTR, _SPC, _ALT}};

// SYMBOL
static char keymapCharSYMBOL[8][6] = {
    // Main
    {_TAB, '!', '@', '#', '$', '%'},
    {_ESC, '~', _MO3, '|', '_', '+'},
    {_GUI, '`', '*', '\\', '-', '='},
    {0, 0, 0, _MO2, _SHF, 0},
    // Secondary (reversed)
    {_BCK, ')', '(', _UP, '&', '^'},
    {_ENT, _HOME, _RGT, _DWN, _LFT, _PGUP},
    {_END, ']', '[', '}', '{', _PGDN},     
    {0, 0, 0, _CTR, _SPC, _ALT}};

// MOUSE
static char keymapCharMOUSE[8][6] = {
    // Main
    {_TAB, 0, 0, 0, 0, 0},
    {_ESC, 0, _MO3, _MAC2, _LCLK, _RCLK},
    {_GUI, 0, 0, _MAC1, 0, 0},
    {0, 0, 0, _MO2, _SHF, 0},
    // Secondary (reversed)
    {_DEL, 0, 0, _UP, 0, 0},
    {_ENT, 0, _RGT, _DWN, _LFT, '+'},
    {0, 0, 0, 0, 0, '-'},
    {0, 0, 0, _CTR, _SPC, _ALT}};

// NUM
static char keymapCharSTARCRAFT[8][6] = {
    // Main
    {_TAB, 'Q', '6', '5', '4', '1'},
    {_ESC, 'A', 'S', 'D', 'F', '2'},
    {_GUI, 'Z', 'X', 'C', 'V', '3'},
    {0, 0, 0, _CTR, _SHF, _MO1},
    // Secondary (reversed)
    {_BCK, 'P', 'O', 'I', 'U', 'Y'},
    {_ENT, ';', 'L', 'K', 'J', 'H'},
    {'\'', '/', '.', ',', 'M', 'N'},
    {0, 0, 0, _CTR, _SPC, _ALT}};

oled_rotation_t oled_init_user(oled_rotation_t rotation)
{
    oled_clear();
    if (is_keyboard_master())
    {
        return OLED_ROTATION_270;
    }
    else
    {
        return OLED_ROTATION_180;
    }
}

#define L_BASE 0
#define L_LOWER 2
#define L_RAISE 4
#define L_ADJUST 8

// static uint32_t oled_timer    = 0;
static char keylog_str[6] = {};
static uint16_t log_timer = 0;

int currentRow;
int currentCol;

// for keyloggin display
static const char PROGMEM code_to_name[0xFF] = {
//   0    1    2    3    4    5    6    7    8    9    A    B    c    D    E    F
    ' ', ' ', ' ', ' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',  // 0x
    'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2',  // 1x
    '3', '4', '5', '6', '7', '8', '9', '0',  20,  19,  27,  26,  22, '-', '=', '[',  // 2x
    ']','\\', '#', ';','\'', '`', ',', '.', '/', 128, ' ', ' ', ' ', ' ', ' ', ' ',  // 3x
    ' ', ' ', ' ', ' ', ' ', ' ', 'P', 'S', ' ', ' ', ' ', ' ',  16, ' ', ' ', ' ',  // 4x
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // 5x
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // 6x
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // 7x
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // 8x
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // 9x
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // Ax
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // Bx
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // Cx
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // Dx
    'C', 'S', 'A', 'G', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // Ex
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '        // Fx
};

// static char keymapDisplayChar[0xFF] = {
// //   0    1    2    3    4    5    6    7    8    9    A    B    c    D    E    F
//     _NO, ' ', ' ', ' ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',  // 0X
//     'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2',  // 1X
//     '3', '4', '5', '6', '7', '8', '9', '0',_ENT,_ESC,_BCK,_TAB,_SPC, '-', '=', '[',  // 2X
//     ']','\\', '#', ';','\'', '`', ',', '.', '/', 'C', _F1, _F2, _F3, _F4, _F5, _F6,  // 3X
//     _F7, _F8, _F9,_F10,_F11,_F12, 'P', 'S', ' ', ' ', ' ', ' ',_DEL, ' ', ' ',_RGT,  // 4X
//    _LFT,_DWN, _UP, ' ', '/', '*', '-', '+',_ENT, '1', '2', '3', '4', '5', '6', '7',  // 5x
//     '8', '9', '0', '.', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // 6x
//     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // 7x
//     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // 8x
//     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // 9x
//     ' ', ' ', ' ', ' ', ' ',_MO1,_MO2,_MO3, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // Ax
//     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // Bx
//     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // Cx
//     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // Dx
//    _CTR,_SHF,_ALT,_GUI,_CTR,_SHF,_ALT,_GUI, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  // Ex
//     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '        // Fx
// };

// static bool keymapActive[0xFF] = {};
static bool keymapActive[8][6] = {};

void add_keylog(uint16_t keycode)
{
    if ((keycode >= QK_MOD_TAP && keycode <= QK_MOD_TAP_MAX) || (keycode >= QK_LAYER_TAP && keycode <= QK_LAYER_TAP_MAX) || (keycode >= QK_MODS && keycode <= QK_MODS_MAX))
    {
        keycode = keycode & 0xFF;
    }
    else if (keycode > 0xFF)
    {
        keycode = 0;
    }

    for (uint8_t i = 4; i > 0; --i)
    {
        keylog_str[i] = keylog_str[i - 1];
    }

    if (keycode < (sizeof(code_to_name) / sizeof(char)))
    {
        keylog_str[0] = pgm_read_byte(&code_to_name[keycode]);
    }

    log_timer = timer_read();

    //   // update keylog
    //   snprintf(keylog_str, sizeof(keylog_str), "%dx%d, k%2d : %c",
    //            record->event.key.row, record->event.key.col,
    //            keycode, name);
}

void oled_render_logo(void)
{
    // static const char PROGMEM crkbd_logo[] = {
    //     0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94,
    //     0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0, 0xb1, 0xb2, 0xb3, 0xb4,
    //     0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4,
    //     0};
    static const char PROGMEM aperture_logo[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0xc0, 0xe0, 0xf0, 0xf8, 0xf8, 0xf8, 0xf0,
0xe2, 0xe6, 0x4e, 0x0e, 0x1e, 0x3e, 0x3e, 0x7e, 0xfe, 0xf0, 0x00, 0x1c, 0xf8, 0xf0, 0xf0, 0xe0,
0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0xc0, 0xf0, 0xf3, 0xfb, 0xf9, 0x79, 0x39, 0x0d, 0x05, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x07, 0x7f, 0x3f, 0x0f,
0x07, 0xc1, 0xf0, 0x78, 0x1c, 0xfc, 0xfc, 0xf8, 0x00, 0x00, 0x00, 0x00, 0xc0, 0xf8, 0xfc, 0xfc,
0x8c, 0x8c, 0xfc, 0xf8, 0x78, 0x00, 0x00, 0x00, 0xc0, 0xf8, 0xfc, 0xfc, 0xcc, 0xcc, 0xcc, 0x1c,
0x08, 0x00, 0x00, 0xe0, 0xf8, 0xfc, 0x9c, 0x8c, 0xcc, 0xfc, 0x78, 0x78, 0x00, 0x00, 0x00, 0x1c,
0x8c, 0xfc, 0xfc, 0xfc, 0x1c, 0x0c, 0x1c, 0x0c, 0x00, 0x80, 0xf8, 0xfc, 0x7c, 0x08, 0x00, 0x80,
0xf8, 0xfc, 0x7c, 0x08, 0x00, 0x00, 0xc0, 0xf8, 0xfc, 0xfc, 0x8c, 0xcc, 0xfc, 0x78, 0x78, 0x00,
0x00, 0x00, 0x80, 0xf8, 0xfc, 0xfc, 0xcc, 0xcc, 0xcc, 0xdc, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x1f, 0x8f, 0xc3, 0xf1, 0xf8, 0xfe, 0xfc, 0x00, 0x00, 0x80,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x80, 0x88, 0x8e,
0x8f, 0xc7, 0x03, 0x03, 0x03, 0x0f, 0x0f, 0x0f, 0x00, 0x00, 0x00, 0x0c, 0x0f, 0x0f, 0x03, 0x01,
0x81, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x8e, 0x0f, 0x0f, 0x0d, 0x0c, 0x0c, 0x8c, 0x80, 0x80,
0x00, 0x00, 0x0f, 0x0f, 0x8f, 0x81, 0x80, 0x09, 0x0f, 0x0f, 0x0f, 0x80, 0x80, 0x80, 0x00, 0x08,
0x0f, 0x0f, 0x0f, 0x80, 0x00, 0x00, 0x00, 0x80, 0x80, 0x87, 0x8f, 0x0f, 0x1c, 0x1c, 0x8c, 0x8f,
0x87, 0x03, 0x00, 0x00, 0x00, 0x8c, 0x8f, 0x8f, 0x03, 0x00, 0x01, 0x8f, 0x0f, 0x0f, 0x00, 0x80,
0x80, 0x8c, 0x0f, 0x0f, 0x0f, 0x8c, 0x8c, 0x8c, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x07, 0x0f, 0x0f, 0x1f, 0x3f, 0x20, 0x03,
0x7f, 0x7e, 0x7e, 0x7c, 0x78, 0x78, 0x73, 0x63, 0x67, 0x4f, 0x1f, 0x1f, 0x1f, 0x1f, 0x0f, 0x07,
0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x07, 0x08, 0x08, 0x00, 0x00, 0x00, 0x06, 0x05, 0x03, 0x04, 0x00, 0x00, 0x00, 0x0f, 0x0a, 0x0a,
0x05, 0x00, 0x00, 0x07, 0x08, 0x08, 0x08, 0x07, 0x00, 0x00, 0x04, 0x02, 0x02, 0x07, 0x00, 0x00,
0x00, 0x04, 0x03, 0x05, 0x06, 0x08, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x07, 0x08, 0x08,
0x08, 0x07, 0x00, 0x00, 0x00, 0x06, 0x02, 0x02, 0x05, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x0f,
0x0a, 0x0a, 0x00, 0x00, 0x00, 0x05, 0x0a, 0x0a, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};
    // oled_write_P(aperture_logo, false);
    oled_write_raw_P(aperture_logo, sizeof(aperture_logo));
}

void render_layer_state(void)
{
    if (layer_state_is(0))
    {
        oled_write_P(PSTR("BASE "), false);
    }
    else if (layer_state_is(1))
    {
        if (layer_state_is(4))
        {
            oled_write_P(PSTR("STAR "), false);
        }
        else
        {
            oled_write_P(PSTR("NUMBR"), false);
        }
    }
    else if (layer_state_is(2))
    {
        if (layer_state_is(3))
        {
            oled_write_P(PSTR("MOUSE"), false);
        }
        else
        {
            oled_write_P(PSTR("SYMBL"), false);
        }
    }
    else if (layer_state_is(4))
    {
        oled_write_P(PSTR("STAR "), false);
    }
    else
    {
        oled_write_P(PSTR("?????"), false);
    }
}

void render_mod_status(uint8_t modifiers)
{
    oled_write_P(PSTR("MODS"), true);
    // oled_write_P(PSTR("-----"), false);
    oled_write_P(PSTR("S"), (modifiers & MOD_MASK_SHIFT));
    oled_write_P(PSTR("C"), (modifiers & MOD_MASK_CTRL));
    oled_write_P(PSTR("A"), (modifiers & MOD_MASK_ALT));
    oled_write_ln_P(PSTR("G"), (modifiers & MOD_MASK_GUI));
    // oled_write_ln_P(PSTR(" "), false);
}

void render_keylogger_status(void)
{
    oled_write_P(PSTR("LOG  "), true);
    // oled_write_P(PSTR("-----"), false);
    oled_write(keylog_str, false);
}

void setActive(int row, int col)
{
    keymapActive[row][col] = true;
}

void setInactive(int row, int col)
{
    keymapActive[row][col] = false;
}

void renderKeyBASE(int row, int col)
{
    oled_write_char(keymapCharBASE[row][col], keymapActive[row][col]);
}

void renderKeyNUM(int row, int col)
{
    oled_write_char(keymapCharNUM[row][col], keymapActive[row][col]);
}

void renderKeySYMBOL(int row, int col)
{
    oled_write_char(keymapCharSYMBOL[row][col], keymapActive[row][col]);
}

void renderKeyMOUSE(int row, int col)
{
    oled_write_char(keymapCharMOUSE[row][col], keymapActive[row][col]);
}

void renderKeySTARCRAFT(int row, int col)
{
    oled_write_char(keymapCharSTARCRAFT[row][col], keymapActive[row][col]);
}

// void showKeycode(uint16_t keycode) {
//     oled_write_char(keymapDisplayChar[keycode], keymapActive[keycode]);
// }

// void showMultipleKeycodes(uint16_t keycodes[][5])
// {
//     for (int i = 0; i < 5; i++)
//     {
//         for (int j = 0; j < 5; j++)
//         {
//             showKeycode(keycodes[i][j]);
//         }
//     }
// }

// layer_state_t currentState;
// uint8_t currentLayerState;

void renderDivider(void)
{
    oled_write_char(140, false);
    oled_write_char(140, false);
    oled_write_char(140, false);
    oled_write_char(140, false);
    oled_write_char(140, false);
}

static const int rowCount = 7;
static const int colCount = 5;
void render_keymap(void)
{
    renderDivider();

    if (layer_state_is(1))
    {
        if (layer_state_is(4))
        {
            ////////////////////////////////////////////////////////////
            // Left Number
            renderKeyNUM(2, 0); // LGUI
            renderKeyNUM(1, 0); // ESCAPE
            renderKeyNUM(0, 0); // TAB
            oled_write_P(PSTR("  "), false);
            for (int row = 0; row < 4; row++)
            {
                for (int col = 1; col < (colCount + 1); col++)
                {
                    renderKeySTARCRAFT(row, col);
                }
            }

            renderDivider();

            // Secondary
            oled_write_P(PSTR("  "), false);
            renderKeyNUM(4, 0); // BACKSPACE
            renderKeyNUM(5, 0); // ENTER
            renderKeyNUM(6, 0); // QUOTE
            for (int row = 4; row < (rowCount + 1); row++)
            {
                for (int col = colCount; col >= 1; col--)
                {
                    renderKeySTARCRAFT(row, col);
                }
            }

            // showMultipleKeycodes(mainKeymapNUM);
            // renderDivider();
            // // oled_advance_page(false);
            // showMultipleKeycodes(secondaryKeymapNUM);
        }
        ////////////////////////////////////////////////////////////
        else // NUM
        {
            // Main
            renderKeyNUM(2, 0); // LGUI
            renderKeyNUM(1, 0); // ESCAPE
            renderKeyNUM(0, 0); // TAB
            oled_write_P(PSTR("  "), false);
            for (int row = 0; row < 4; row++)
            {
                for (int col = 1; col < (colCount + 1); col++)
                {
                    renderKeyNUM(row, col);
                }
            }

            renderDivider();

            // Secondary
            oled_write_P(PSTR("  "), false);
            renderKeyNUM(4, 0); // BACKSPACE
            renderKeyNUM(5, 0); // ENTER
            renderKeyNUM(6, 0); // QUOTE
            for (int row = 4; row < (rowCount + 1); row++)
            {
                for (int col = colCount; col >= 1; col--)
                {
                    renderKeyNUM(row, col);
                }
            }

            // showMultipleKeycodes(mainKeymapNUM);
            // renderDivider();
            // // oled_advance_page(false);
            // showMultipleKeycodes(secondaryKeymapNUM);
        }
    }
    else if (layer_state_is(2))
    {
        ////////////////////////////////////////////////////////////
        if (layer_state_is(3)) // MOUSE
        {

            // Main
            renderKeyMOUSE(2, 0); // LGUI
            renderKeyMOUSE(1, 0); // ESCAPE
            renderKeyMOUSE(0, 0); // TAB
            oled_write_P(PSTR("  "), false);
            for (int row = 0; row < 4; row++)
            {
                for (int col = 1; col < (colCount + 1); col++)
                {
                    renderKeyMOUSE(row, col);
                }
            }

            renderDivider();

            // Secondary
            oled_write_P(PSTR("  "), false);
            renderKeyMOUSE(4, 0); // BACKSPACE
            renderKeyMOUSE(5, 0); // ENTER
            renderKeyMOUSE(6, 0); // QUOTE
            for (int row = 4; row < (rowCount + 1); row++)
            {
                for (int col = colCount; col >= 1; col--)
                {
                    renderKeyMOUSE(row, col);
                }
            }

            // showMultipleKeycodes(mainKeymapMOUSE);
            // renderDivider();
            // // oled_advance_page(false);
            // showMultipleKeycodes(secondaryKeymapMOUSE);
        }
        ////////////////////////////////////////////////////////////
        else // SYMBOL
        {

            // Main
            renderKeySYMBOL(2, 0); // LGUI
            renderKeySYMBOL(1, 0); // ESCAPE
            renderKeySYMBOL(0, 0); // TAB
            oled_write_P(PSTR("  "), false);
            for (int row = 0; row < 4; row++)
            {
                for (int col = 1; col < (colCount + 1); col++)
                {
                    renderKeySYMBOL(row, col);
                }
            }

            renderDivider();

            // Secondary
            oled_write_P(PSTR("  "), false);
            renderKeySYMBOL(4, 0); // BACKSPACE
            renderKeySYMBOL(5, 0); // ENTER
            renderKeySYMBOL(6, 0); // QUOTE
            for (int row = 4; row < (rowCount + 1); row++)
            {
                for (int col = colCount; col >= 1; col--)
                {
                    renderKeySYMBOL(row, col);
                }
            }

            // showMultipleKeycodes(mainKeymapSYMBOL);
            // renderDivider();
            // // oled_advance_page(false);
            // showMultipleKeycodes(secondaryKeymapSYMBOL);
        }
    }
    else if (layer_state_is(4))
    {
        ////////////////////////////////////////////////////////////
        // Left Number
        renderKeyNUM(2, 0); // LGUI
        renderKeyNUM(1, 0); // ESCAPE
        renderKeyNUM(0, 0); // TAB
        oled_write_P(PSTR("  "), false);
        for (int row = 0; row < 4; row++)
        {
            for (int col = 1; col < (colCount + 1); col++)
            {
                renderKeySTARCRAFT(row, col);
            }
        }

        renderDivider();

        // Secondary
        oled_write_P(PSTR("  "), false);
        renderKeyNUM(4, 0); // BACKSPACE
        renderKeyNUM(5, 0); // ENTER
        renderKeyNUM(6, 0); // QUOTE
        for (int row = 4; row < (rowCount + 1); row++)
        {
            for (int col = colCount; col >= 1; col--)
            {
                renderKeySTARCRAFT(row, col);
            }
        }

        // showMultipleKeycodes(mainKeymapNUM);
        // renderDivider();
        // // oled_advance_page(false);
        // showMultipleKeycodes(secondaryKeymapNUM);
    }
    else
    {
        ////////////////////////////////////////////////////////////
        // Main
        renderKeyBASE(2, 0); // LGUI
        renderKeyBASE(1, 0); // ESCAPE
        renderKeyBASE(0, 0); // TAB
        oled_write_P(PSTR("  "), false);
        for (int row = 0; row < 4; row++)
        {
            for (int col = 1; col < (colCount + 1); col++)
            {
                renderKeyBASE(row, col);
            }
        }

        renderDivider();

        // Secondary
        oled_write_P(PSTR("  "), false);
        renderKeyBASE(4, 0); // BACKSPACE
        renderKeyBASE(5, 0); // ENTER
        renderKeyBASE(6, 0); // QUOTE
        for (int row = 4; row < (rowCount + 1); row++)
        {
            for (int col = colCount; col >= 1; col--)
            {
                renderKeyBASE(row, col);
            }
        }

        // showMultipleKeycodes(mainKeymapBASE);
        // renderDivider();
        // // oled_advance_page(false);
        // showMultipleKeycodes(secondaryKeymapBASE);
    }

    renderDivider();
}

void update_log(void)
{
    if (timer_elapsed(log_timer) > 250)
    {
        add_keylog(0);
    }
}

char row[24] = {};
char col[24] = {};

void render_status_main(void)
{
    // 16 lines top to bottom
    render_layer_state();
    // oled_advance_page(false);
    render_keymap();
    // render_mod_status(get_mods() | get_oneshot_mods());
    // render_keylogger_status();

    snprintf(row, sizeof(row), "r%d ", currentRow);
    snprintf(col, sizeof(col), "c%d", currentCol);
    oled_write(row, false);
    oled_write(col, false);
}

// layer_state_t layer_state_set_user(layer_state_t state)
// {
//     // setInactive(KC_MOD1);
//     setInactive(3, 5); // MOD1
//     // setInactive(KC_MOD2);
//     setInactive(3, 3); // MOD2
//     // setInactive(KC_MOD3);
//     setInactive(1, 2); // MOD3

//     switch (get_highest_layer(state))
//     {
//     case 1:
//         // NUM
//         setActive(3, 5);
//         // setActive(KC_MOD1);
//         break;
//     case 2:
//         // SYMBOL
//         setActive(3, 3);
//         // setActive(KC_MOD2);
//         if (layer_state_is(3))
//         {
//             // MOUSE
//             setActive(1, 2);
//             // setActive(KC_MOD3);
//         }
//         break;
//     case 3:
//         // MOUSE
//         setActive(1, 2);
//         // setActive(KC_MOD3);
//         if (layer_state_is(2))
//         {
//             // SYMBOL
//             setActive(3, 3);
//             // setActive(KC_MOD2);
//         }
//         break;
//     default: //  for any other layers, or the default layer
//         break;
//     }
//     return state;
// }

// void setLayerModsActive(void)
// {
//     // setInactive(KC_MOD1);
//     setInactive(3, 5); // MOD1
//     // setInactive(KC_MOD2);
//     setInactive(3, 3); // MOD2
//     // setInactive(KC_MOD3);
//     setInactive(1, 2); // MOD3
//     if (layer_state_is(0))
//     {
//     }
//     else if (layer_state_is(1))
//     {
//         // NUM
//         setActive(3, 5);
//         // setActive(KC_MOD1);
//     }
//     else if (layer_state_is(2))
//     {
//         if (layer_state_is(3))
//         {
//             // MOUSE
//             setActive(1, 2);
//             // setActive(KC_MOD3);
//         }
//         else
//         {
//             // SYMBOL
//             setActive(3, 3);
//             // setActive(KC_MOD2);
//         }
//     }
//     else
//     {
//     }
// }

void displayCurrentKeypress(bool toggle, keyrecord_t *record)
{
    if (toggle)
    {
        setActive(record->event.key.row, record->event.key.col);
        currentRow = record->event.key.row;
        currentCol = record->event.key.col;
    }
    else
    {
        setInactive(record->event.key.row, record->event.key.col);
    }
}

#define MODS_CTRL_MASK (MOD_BIT(KC_LSFT))
static bool bsdel_mods = false;
bool process_record_user(uint16_t keycode, keyrecord_t *record)
{
    switch (keycode)
    {
    case M_BSDEL:
    { // Backspace delete shift macro
        uint8_t kc = KC_BSPC;

        if (record->event.pressed)
        {
            if (keyboard_report->mods & MODS_CTRL_MASK)
            {
                kc = KC_DEL;
                bsdel_mods = true;
            }
            else
            {
                bsdel_mods = false;
            }

            if (bsdel_mods)
            {
                unregister_code(KC_LSFT);
            }

            register_code(kc);
            del_bspc_lastcode = kc;

            if (bsdel_mods)
            {
                register_code(KC_LSFT);
            }
        }
        else
        {
            unregister_code(del_bspc_lastcode);
        }

        break;
    }
    }

    if (record->event.pressed)
    { // on key down
#ifdef OLED_ENABLE
        // oled_timer = timer_read32();
        displayCurrentKeypress(true, record);
#endif
#ifndef SPLIT_KEYBOARD
        if (keycode == RESET && !is_keyboard_master())
        {
            return false;
        }
#endif
    }
    else
    { // on key up
#ifdef OLED_ENABLE
        displayCurrentKeypress(false, record);
#endif
    }

    return true;
}

bool oled_task_user(void)
{
    // update_log();

    if (is_keyboard_master())
    {
        render_status_main();
        // oled_render_layer_state();
        // oled_render_keylog();
    }
    else
    {
        oled_render_logo();
    }

    return false;
}

#endif // OLED_ENABLE
